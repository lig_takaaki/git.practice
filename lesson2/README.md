デザイナー向けのGit講座 #2

##テーマ: ブランチとブランチのマージ、リセット
###目標: ブランチの切り分け、ブランチのマージ、特定バージョンの復元

##1. ブランチの切り分け

###宿題
`[自分の名前]ブランチを作成して、lesson1/home_workフォルダ内に[自分の名前].htmlを作成して下さい。`

* [詳細](https://bitbucket.org/n0bisuke/git.practice/src/HEAD/lesson1/home_work/README.md?at=master)

#### 例: n0bisukeブランチを作ってn0bisuke.htmlを作成
* [宿題(のびすけ)](https://bitbucket.org/n0bisuke/git.practice/src/HEAD/lesson1/home_work/n0bisuke.html?at=n0bisuke)
![](http://i.gyazo.com/915bd5d761186205fd9389f21b8c4aaa.png)

##2. ブランチのマージ (15〜30分)
(ハンズオン)

##3. 特定のバージョンの復元 (15〜30分)
(ハンズオン)

##4. オートマージ (時間が余ったら)
##5. 画像ファイルの差分 (時間が余ったら)